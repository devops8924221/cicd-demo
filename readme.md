# Functionality per Application
* Prometheus
* HTTP Status Codes
    * 100
    * 200
    * 302
        * ´location´ parameter
    * 401
        * allow login with "user:password"
    * 403
    * 404
    * 500