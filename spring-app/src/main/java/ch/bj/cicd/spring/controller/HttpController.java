package ch.bj.cicd.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("simple-web-controller")
public class HttpController {

	// http://localhost:8080/simple-web-controller/1
	@GetMapping(path = "/{id}", produces = "application/json")
	public @ResponseBody String getString(@PathVariable int id) {
		return "{\"This is String\": \"" + id + "\"}";
	}
}
