package ch.bj.cicd.spring.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("rest")
public class SimpleRestController {

	@GetMapping(path = "/echo") 
	public String getEcho() {
		return "Echo 1 2 3";
	}
	
	// http://localhost:8080/rest/1
	@GetMapping(path = "/{id}", produces = "application/json")
	public String getString(@PathVariable int id) {
		return "{\"This is REST object\": \"" + id + "\"}";
	}
	
}
