package ch.bj.cicd.spring.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping("http")
public class SimpleWebController {

	// http://localhost:8080/http/100
	@GetMapping(path = "/{httpCode}")
	public ResponseEntity<String> getResponse(@PathVariable int httpCode, @RequestParam(required = false) String location) {
		System.out.println(httpCode);
		var httpStatus = HttpStatus.valueOf(httpCode);
		switch(httpCode) {
			case 302:
				var locParam = location;
				// location should be the http status to redirect to
				if (location == null || location.length() == 0) {
					locParam = "200";
				}
				HttpHeaders h = new HttpHeaders();
				h.set(HttpHeaders.LOCATION, locParam);
				ResponseEntity resp = new ResponseEntity<String>(h, httpStatus);
				return resp;
		}
		
		throw new ResponseStatusException(httpStatus);
	}
}
